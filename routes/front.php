<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('Frontend')->group(function(){
    Route::prefix('upload')->group(function(){
        Route::post('/', 'UploadController@upload');
        Route::delete('/', 'UploadController@delete');
        Route::post('/complete', 'UploadController@complete');
    });

//    Route::middleware('auth:api')->group(function(){
//
//    });
});
