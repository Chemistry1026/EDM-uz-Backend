<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Upload;

class Track extends Model
{
    public function files(){
        return $this->morphToMany(Upload::class, 'uploadable');
    }
}
