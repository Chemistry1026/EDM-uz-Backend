<?php

namespace App\Http\Controllers\Frontend;

use App\Http\CM;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Traits\Upload;

class UploadController extends Controller
{
    use Upload;

    public $uploadDir = '';
    public $cm = null;

    public function __construct(CM $CM){
        $this->cm = $CM;
        $this->chunksFolder = public_path($this->chunksFolder);
    }

    public function upload(Request $request){
        $result = $this->handleUpload($this->cm->absolute_current_upload_path());
        $result["uploadName"] = $this->getUploadName();
        return response()->json($result);
    }

    public function complete(Request $request){
        $result = $this->combineChunks($this->cm->absolute_current_upload_path());
        return response()->json($result);
    }

    public function delete(Request $request){
        $result = $this->handleDelete($this->cm->absolute_current_upload_path());
        return response()->json($result);
    }
}
