<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AudioController extends Controller
{
    public $ffmpeg = '';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){
        $ffmpeg = shell_exec('which ffmpeg');
        $this->ffmpeg = ($ffmpeg) ? $ffmpeg : '/usr/local/bin/ffmpeg';
    }

    public function makeLowerTrack($file, $bitrate = 128){
        $pathinfo = pathinfo($file);
        $path = $pathinfo['dirname'];
        $name = $pathinfo['filename'];
        $ext = $pathinfo['extension'];

        shell_exec("{$this->ffmpeg} -i '$file' -vn -ar 44100 -ac 2 -b:a {$bitrate}k '$path/$name-128.$ext' > /dev/null 2>/dev/null &");
    }

    public function makeWaveform($file){
        $pathinfo = pathinfo($file);
        $path = $pathinfo['dirname'];
        $name = $pathinfo['filename'];
        $img = "$path/$name-waveform.png";

        shell_exec("{$this->ffmpeg} -y -i '$file' -filter_complex 'aformat=channel_layouts=mono,compand,showwavespic=s=600x120,crop=in_w:in_h/2:0:0' -c:v png -frames:v 1 '$img'");

        return $this->getWaveformDots($img);
    }

    public function getWaveformDots($file){
        $pathinfo = pathinfo($file);
        $path = $pathinfo['dirname'];
        $name = $pathinfo['filename'];
        $json = "$path/$name-waveform.json";

        $a = imagecreatefrompng($file);
        $i = 0;
        $h = '60';
// horizontal movener
        while ( $i < 600 ) {

            // vertical movener
            $y  = $h-1;
            $c = 0;
            while ( $c < $h ) {

//                echo imagecolorat($a, $i, $c )."\n"; // test color
                if(imagecolorat($a, $i, $c ) == "16711680") {
                    $arr[$i] =  $c;
                    break;
                } else {
                    $arr[$i] =  $y;
                }
                $c++;
            }
            $i++;
        };

        $data = [
            'data' => $arr,
        ];

        return file_put_contents($json, json_encode($data));
    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
