<?php

namespace App\Http;

class CM {
    //Uploader
    public static function get_current_month_year() {
        return date('Y-m');
    }

    public static function upload_path() {
        return "public/uploads/";
    }

    public static function chunks_path() {
        return "public/chunks/";
    }

    public function current_upload_path($file = '') {
        return self::upload_path().self::get_current_month_year()."/{$file}";
    }

    public function absolute_upload_path($file = '') {
        return storage_path("app/".self::upload_path().$file);
    }
    public function absolute_current_upload_path($file = '') {
        return storage_path("app/".$this->current_upload_path($file));
    }

    public function absolute_chunks_path($file = '') {
        return storage_path("app/".self::chunks_path().$file);
    }

    //Json
    public function json_data($data, $message = '', $status = 200){
        return response()->json([
            'data' => $data,
            'message' => $message,
        ], $status);
    }

//    public function current_upload_path($file = ''){
//        return $this->upload_path(self::get_current_month_year()."/{$file}");
//    }
}
