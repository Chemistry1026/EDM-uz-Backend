<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Upload extends Model
{
    public function uploadable(){
        return $this->morphTo();
    }
}
